#!/bin/bash

if [ "$PROD_UPDATE" = "false" ]
  then echo "Prod sites not ready for update"; exit 0;
fi

source ./wp-cli-aliases.sh

cd "${JENKINS_HOME}/.wp-cli"

for i in "${prod_aliases[@]}"
do
  echo "starting db export for $i"

  tmp_file_name="${i//@/}-tmp"

  # check if file exists on remote server and delete it
  echo "deleting backup files if they exist"
  ssh comms "if [ -f /home/deploy/backups/$tmp_file_name.sql.gz ]; then rm /home/deploy/backups/$tmp_file_name.sql.gz; fi"

  echo "exporting db for $i"
  wp $i db export \
    $tmp_file_name.sql \
    --single-transaction \
    --quick \
    --lock-tables=false \
    --skip-themes \
    --skip-plugins

  echo "compressing db and backing up"
  ssh comms "gzip -9 -c $tmp_file_name.sql > $tmp_file_name.sql.gz && rm $tmp_file_name.sql && mv $tmp_file_name.sql.gz /home/deploy/backups/$tmp_file_name.sql.gz"

  echo "backup complete. file name: $tmp_file_name.sql.gz moved to backups directory"

  echo "updating wp core for $i"
  wp $i core update --skip-plugins --skip-themes

  echo "updating core version in db for $i"
  if [ "$i" = "@prod-uconn" ]
    then wp $i core update-db --network --skip-plugins --skip-themes
    else wp $i core update-db --skip-plugins --skip-themes
  fi

done