#!/bin/bash

if [ "$ADMISSIONS_UPDATE" = "false" ]
  then echo "Admissions not included for update"; exit 0;
fi

source ./wp-cli-aliases.sh

cd "${JENKINS_HOME}/.wp-cli"

echo "starting db export for @prod-admissions"

tmp_file_name="prod-admissions-tmp"

echo "deleting backup files if they exist"
ssh comms "if [ -f /home/deploy/backups/$tmp_file_name.sql.gz ]; then rm /home/deploy/backups/$tmp_file_name.sql.gz; fi"

echo "exporting db for @prod-admissions"
wp @prod-admissions db export \
  $tmp_file_name.sql \
  --single-transaction \
  --quick \
  --lock-tables=false \
  --skip-themes \
  --skip-plugins

echo "compressing db and backing up"
ssh comms "gzip -9 -c $tmp_file_name.sql > $tmp_file_name.sql.gz && rm $tmp_file_name.sql && mv $tmp_file_name.sql.gz /home/deploy/backups/$tmp_file_name.sql.gz"

echo "backup complete. file name: $tmp_file_name.sql.gz moved to backups directory"

echo "updating wp core for @prod-admissions"
wp @prod-admissions core update --skip-plugins --skip-themes

echo "updating core version in db for @prod-admissions"
wp @prod-admissions core update-db --skip-plugins --skip-themes