pipeline {
  agent any
  environment {
    DEV_BRANCH = "develop"
    PROD_BRANCH = "main"
  }
  // handle rsync dry runs
  parameters {
    booleanParam(
      name: 'ADMISSIONS_UPDATE',
      defaultValue: false,
      description: 'Allows wp core to be updated on the admissions site'
    )
    booleanParam(
      name: 'PROD_UPDATE',
      defaultValue: false,
      description: 'Allows wp core to be updated on prod sites _except_ admissions'
    )
  }
  stages {
    // checkout from git and skip if the "ci skip" message is present in the commit
    stage('Checkout') {
      steps {
        // do not delete builds. there's a breaking issue there
        // https://issues.jenkins.io/browse/JENKINS-66843
        scmSkip(deleteBuild: false)
        // relies on the global notification library
        // https://bitbucket.org/ucomm/jenkins-send-notifications/src/main/
        sendNotifications 'STARTED'
      }
    }
    stage('Dev Update') {
      when {
        branch "${DEV_BRANCH}"
      }
      steps {
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          configFileProvider([
            configFile(fileId: 'wp-cli-aliases', targetLocation: './wp-cli-aliases.sh')
          ]) {
            sh "${WORKSPACE}/ci-scripts/dev-update.sh"
            sh "${WORKSPACE}/ci-scripts/staging-update.sh"
          }
        }
      }
      post {
        failure {
          sendNotifications "DEV PUSH FAILED"
        }
      }
    }
    stage('Prod Update') {
      when {
        branch "${PROD_BRANCH}"
      }
      steps {
        catchError(buildResult: 'SUCCESS', stageResult: 'FAILURE') {
          configFileProvider([
            configFile(fileId: 'wp-cli-aliases', targetLocation: './wp-cli-aliases.sh')
          ]) {
            sh "${WORKSPACE}/ci-scripts/prod-update.sh"
            sh "${WORKSPACE}/ci-scripts/admissions-update.sh"
          }
        }
      }
      post {
        failure {
          sendNotifications("FAILURE - Push to prod")
        }
        success {
          sendNotifications("SUCCESS", "#comm0-updates", [
            [
              type: "section",
              text: [
                type: "mrkdwn",
                text: ":tada: WP core updated"
              ]              
            ]
          ])
        }
      }
    }
  }
  post {
    // send slack notifications when the project finishes
    aborted {
      sendNotifications 'ABORTED'
    }
    success {
      sendNotifications 'SUCCESSFUL'
    }
    failure {
      sendNotifications 'FAILED'
    }
  }
}