# WP Core updater

This package is just meant to provide an easy way to update WP core on staging0 and comm0 servers. It is not meant for active development.

## Use
This project is managed by wp cli and jenkins. Updates will be made to dev/staging sites on every push to the `develop` branch _unless_ `[ci skip]` is added to the commit message.

Otherwise, go to the appropriate job on jenkins and click the "build" button.